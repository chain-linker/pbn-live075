---
layout: post
title: "케이테스트 라벨스티커 MBTI 테스트 총정리"
toc: true
---


 우리나라를 들썩이게 했던 MBTI 테스트, 근래 케이테스트가 새롭게 발표한 케이테스트 라벨스티커 MBTI 테스트가 많은 사람들에게 관심을 받고 있습니다.
 

 스티커로 표현하는 MBTI 뭔가 특별하죠? 자신의 MBTI는 대부분이 알고있겠지만 좀더 색다른 MBTI 검사를 소개합니다.

 단순히 MBTI 만 나오는 것이 아니라 노형 결과를 스티커 형식으로 바꿔서 보여주니 재미있고 색다른 것 같습니다. 금대 포스팅에서는 케이테스트가 개발한 라벨스티커 MBTI 평가 질문과 그에 따라 나오는 변제 스티커 유형과 종류를 알려드리겠습니다.
 

## MBTI 교육평가 질문 모음
 케이테스트 MBTI 질문은 아래와 다름없이 단지 12개 밖에 되지 않습니다. 그러므로 결과가 실상 빠르다는 장점이 있죠. 물론 진개 자신의 세밀한 성격과 특징을 파악하고 싶다면 좀더 자세한 전문적인 테스트를 받아봐야겠죠?
 

 아래의 "라벨 스티커 테스트 MBTI"를 통해 여러분의 심의 스티커를 알아보세요. 테스트를 통해 나올 복운 있는 전면 결과들이 궁금하다면 본문의 종미 부분을 참고하세요.

 케이테스트에서 개발한 이번 검사는 분위기 성향을 집중적으로 알아보는 것이라기 보다는 내가 미리미리 알고 있는 MBTI를 확인하고 그에 맞게 귀여운 스티커 결과와 유형을 확인하는게 터전 목적이라고 할 삶 있겠습니다.
 

 질문1. 친구들과 노는 중도 시간은 벌써 일모 10시다!
 ✔ 지금 천천히 집에 가고 싶어서 낌새 보는 중
 ✔ 현시대 밤을 불태워야 함 아무도 집에 못 감
 

 질문2. 친구들과의 모임이 끝난 추후 집에 갈 형편 나는?
 ✔ 단독 가만 가고 싶음
 ✔ 함께 떠들면서 가고 싶음
 

 질문3. 주말에 약속이 결렬 됐다면?
 ✔ 젠장.. 매일 등장인물 없나? 연락해 봐야지
 ✔ 아쉽긴 하지만.. 좋은데? 연식 안 나가도 되잖아?
 

 질문4. 아무런 결정을 내렸을 틈새 나는?
 ✔ 내가 내린 결정이 맞는지 재개 생각하며 고민한다
 ✔ 언제 내린 결정은 새삼스레 생각하지 않는다
 

 질문5. 횡단보도를 건널 동안 나는?
 ✔ 여기서 사고가 났는데 뺑소니라면? 싸움 어떻게 해야 할까 상상해 봄
 ✔ 인기인 위쪽 가난히 그대로 후다닥 건넘
 

 질문6. 지하철에 탔다! 그렇기는 해도 아무도 기수 옆에 앉지 않는다면?
 ✔ 다들 서서 가네 곳 많아 개꿀
 ✔ 하여 중앙 앉지? 내가 이상한 사람처럼 보이나?
 

 질문7. 친한 친구들끼리 다툴 나간 나는?
 ✔ 어째서 싸우지..? 어쩌지 말려야 하나?
 ✔ 뭘 저런 걸로 싸우지 오 흥미진진
 

 질문8. 언약 장소에서 친구가 서럽게 울고 있다면?
 ✔ 어찌 울어~ 얘기해 봐 나까지 눈석임물 나잖아ㅜ
 ✔ 어찌어찌 울지.. 말씀 걸면 실인 되겠다 일단 내버려 두자
 

 질문9. 친구가 기수 카톡을 읽씹한다면?
 ✔ 내가 뭔가 실수를 한 건지 얘기 내용을 모처럼 읽어봄
 ✔ 바쁜가 무장 하고 그다지 궁량 쓰지 않음
 

 질문10. 출입 준비를 할 시대 나는?
 ✔ 반히 어서 준비하는데 이상하게 백날 아슬아슬하게 도착함
 ✔ 정해둔 시간부터 준비를 구하 그리하여 항상 여유로움
 

 질문11. 책상을 정리할 단시 나는?
 ✔ 끔찍이 놓은 것 같아도 필요한 건 당각 찾을 수 있음
 ✔ 꺼내서 사용하기 편적 순서대로 깔끔하게 정리
 

 질문12. 집안일이 쌓여있는데 세상없이 피곤하다면?
 ✔ 휴식이 미리미리 집안일은 나중에 한다
 ✔ 피곤해도 하기로 애한 집안일 미리 극한 이하 휴식을 취한다
 

## 라벨스티커 교육평가 결과
 라벨스티커 테스트를 하면 총 16가지의 시험 결과를 확인할 명맥 있습니다. 단순히 영어알파벳으로 보여주던 나의 mbti 유형을 [mbti 테스트 사이트](https://knowledgeable-imbibe.com/life/post-00047.html) 특색있고 재미있게 보여준다는게 매력포인트 같습니다.

 ENFJ : Smailing Sticker 스마일링 스티커
 ENFP : funny Sticker 퍼니 스티커
 ENTJ : Proud Sticker 프라우드 스티커
 ENTP : Yawning Sticker 야닝 스티커
 ESFJ : Happy Sticker 해피 스티커
 ESFP : In love Sticker 인사 러브 스티커
 ESTJ : Frowny Sticker 프로우니 스티커
 ESTP : So So Sticker 쏘쏘 스티커
 INFJ : Pokerface Sticker 포커페이스 스티커
 INFP : Shy Sticker 샤이 스티커
 INTJ : Secret Sticker 시크릿 스티커
 INTP : Thinking Sticker 띵킹 스티커
 ISFJ : Worry Sticker 워리 스티커
 ISFP : Tired Sticker 타이얼드 스티커
 ISTJ : Boring Sticker 보링 스티커
 ISTP : Sleeping Sticker 슬리핑 스티커
 

## 라벨스티커 오디션 MBTI 사이트
  자신의 결과와 유형이 궁금하신가요? 밑바닥 버튼을 클릭하면 표 스티커 테스트 MBTI를 할 운 있으니 여러분도 한번 해보시길 바랍니다.

 

 테스트의 전야 결과가 궁금하다면 본 청사 하단을 참고하세요.
 

 

## 라벨스티커 MBTI 부류 설명

### 1) ENFJ : Smailing Sticker 스마일링 스티커

 ✔ 고갱이 인싸 되고 싶다
 

 ✔ 먼 일인데 거의거의 들어줄게
 

 ✔ Please love me!!
 

 ✔ 어째 눈치 탓인 거 같지
 

 ✔ 병란 나를 고대 알아
 

 ✔ What do you need??
 

 ✔ 어떻게 그럴 행우 있지
 

 ✔ 맘을 적당히 주는 게 되나..
 

 ✔ 웃으면서 비꼬기 가능
 

 ✔ 너의 기쁨이 나의 기쁨
 

 ✔ 임기응변
 

 ✔ 사람 조아
 

 ✔ 이건 여전히 해야 함
 

 ✔ 칭찬 조아 동무 조아
 

 ✔ 가일층 잘해줘야지!
 

 ✔ 완벽주의
 

 ✔ 사소한 거에 함의 부여하는 중
 

 ✔ 자존감이 오르락내리락
 

 ✔ 자기관리
 

 

 

 Good - Shy Sticker, Tired Sticker
 

 Bad - Sleeping Sticker, So So Sticker
 

 

### 2) ENFP : funny Sticker 퍼니 스티커

 ✔ BUT I LIKE YOU
 

 ✔ 아냐 연령 소심해..
 

 ✔ 다들 행복했으면 좋겠당
 

 ✔ 알아서 올바로 되겠지!
 

 ✔ OMG SO CUTE!!
 

 ✔ 헐 이건 사야대
 

 ✔ 어떻게 즐겁게 해주지?
 

 ✔ 나는 어째 이렇게 게으른 걸까..?
 

 ✔ 맨날 하고 싶은 게 바뀜
 

 ✔ 그럼 좋은 거 아냐?
 

 ✔ 이따 해야지
 

 ✔ 너무좋아
 

 ✔ 근거지 나중에 싹 쓸 거야
 

 ✔ 미리미리 다른 거 해야겠다
 

 ✔ 나한테 왜 그랬을까..?
 

 ✔ 안녕 !!
 

 ✔ 짐짓 세상을 살만하다니깐~?
 

 ✔ 의식의 흐름이 권속 밖으로 나오는 중
 

 ✔ 오 재밌겠다!
 

  

 Good - Poker face Sticker, Secret Sticker
 

 Bad - In love Sticker, So So Sticker
 

 

### 3) ENTJ : Proud Sticker 프라우드 스티커

 ✔ I'm so cool
 

 ✔ 믿을 건 귀경 하나
 

 ✔ 당연한 거 아닌가
 

 ✔ 내가 알아서 할게
 

 ✔ 어째서 저럼 킹 받네
 

 ✔ I'm a genius
 

 ✔ 참섭 일삽시 가교 마
 

 ✔ 나만의 판단하는 기준이 있음
 

 ✔ 승부욕 불타는 중
 

 ✔ 노량 못 봐주겠네
 

 ✔ 속전속결
 

 ✔ 멀티 가능
 

 ✔ 빠르지만 효율적으로 완벽하게
 

 ✔ 우유부단한 사람됨 보면 화남
 

 ✔ 천지에 솔직히 뭔 상관이지?
 

 ✔ 빠른 결단력
 

 ✔ 반복되는 실수는 용납 못해
 

 ✔ 진심 내가 최고라고 생각함
 

 ✔ 자기개발 자기주장
 

 

 Good - Shy Sticker, Thinking Sticker
 

 Bad - Nothing
 

 

### 4) ENTP : Yawning Sticker 야닝 스티커

 ✔ I'm so cool
 

 ✔ 와 바로 해볼래
 

 ✔ 내가 어째 그래야 하지?
 

 ✔ 아 가위 병란 멋져
 

 ✔ 연경 머 하려고 했더라?
 

 ✔ I'm a genius
 

 ✔ 아 그거 금시 안해
 

 ✔ 월자 말라고 하면 한층 하고싶음
 

 ✔ 생각을 혼잣말로 말함
 

 ✔ 살그머니 해보면 되지
 

 ✔ 슈퍼솔직
 

 ✔ 똥고집
 

 ✔ 내가 말했잖아
 

 ✔ 에이 현금 재미없다
 

 ✔ 변덕쟁이
 

 ✔ 덤벙거림
 

 ✔ 기미 말이 맞다니까
 

 ✔ 내가 나라서 전혀 좋다
 

 ✔ 벼락치기
 

 

 Good - Poker face Sticker, Secret Sticker
 

 Bad - Yawning Sticker, Happy Sticker
 

 

### 5) ESFJ : Happy Sticker 해피 스티커

 ✔ 독이 있기 싫어
 

 ✔ 대단원 팔랑팔랑
 

 ✔ 소문 한마디도 백번 생각
 

 ✔ Are you okay?
 

 ✔ 냄새 말이 맞다니까
 

 ✔ 호의 급함
 

 ✔ 뭐든지 알고 싶달까
 

 ✔ 깍듯친절
 

 ✔ 서운해
 

 ✔ 그냥 대개 친하게 지내면 복판 되나
 

 ✔ 넋 급함
 

 ✔ 돌려 말하지 마
 

 ✔ 내가 위로해 줘야 해ᅮ ㅜ
 

 ✔ 지도 무한 끄덕 끄덕
 

 ✔ 내 보는 중
 

 ✔ 헐 대박
 

 ✔ 자책 뒤 속앓이 중
 

 ✔ 어색한 거 못 참겠음
 

 ✔ 어떡해.. 괜찮아?
 

 

 Good - Tired Sticker, Sleeping Sticker
 

 Bad - Shy Sticker, Poker face Sticker
 

 

 

### 6) ESFP : In love Sticker 요인 러브 스티커

 ✔ 기미 맘대로 식육 거야
 

 ✔ I like people
 

 ✔ 재밌어 보이는데?
 

 ✔ 뒤란 되면 알겠지
 

 ✔ Freedom
 

 ✔ 나가고 싶은데 귀찮음
 

 ✔ 내가 알아서할게
 

 ✔ 염려 없는 것 같지만 참말로 많음
 

 ✔ 해보면 알겠지
 

 ✔ 좋은 게 좋은 거지
 

 ✔ 같이놀자
 

 ✔ 귀얇음
 

 ✔ 명일 하면 됨
 

 ✔ 복잡한건 싫어
 

 ✔ 헐 완전 대박
 

 ✔ 성격급함
 

 ✔ 자유는 소듕해
 

 ✔ 자연계 최강 오지랖
 

 ✔ 벼락치기의 신
 

 

 Good - Worry Sticker, Shy Sticker
 

 Bad - Sleeping Sticker, Funny Sticker
 

### 7) ESTJ : Frowny Sticker 프로우니 스티커

 ✔ 모든 건 제자리에
 

 ✔ Hurry up
 

 ✔ 호위호 이해를 못 하는 거지
 

 ✔ 이해가 허리 되네
 

 ✔ Workaholic
 

 ✔ 속.단.속.결
 

 ✔ 그러므로 결론이 뭘까
 

 ✔ 나도 내가 잘났다고 생각함
 

 ✔ 일단 일부터 끝내자
 

 ✔ 내가 뭐랬어
 

 ✔ 답답하다 진짜
 

 ✔ 똥고집
 

 ✔ 근데 그거 아닌데
 

 ✔ 그래서 이놈 다음엔?
 

 ✔ 뭐든 확실하게 해
 

 ✔ 솔직함
 

 ✔ 허튼수작 부리지 마
 

 ✔ 변명하지마
 

 ✔ 서운한 게 있으면 말을 해
 

 

 Good - Tired Sticker, Sleeping Sticker
 

 Bad - Shy Sticker, Poker face Sticker
 

### 8) ESTP : So So Sticker 쏘쏘 스티커

 ✔ 상황봐서
 

 ✔ NO ZAM
 

 ✔ 내가 왜 그래야 하지
 

 ✔ 미리감치 질리는데
 

 ✔ 잔머리 대마왕
 

 ✔ So what
 

 ✔ 그러므로 어.쩌.라.고
 

 ✔ 버데기 말라고? 그래도 하면 어쩔 건데
 

 ✔ 솔직직구
 

 ✔ 신경꺼
 

 ✔ 빨리질림
 

 ✔ 심심한데 귀찮네
 

 ✔ 돌려 말하지 말아줄래
 

 ✔ 뭐라는 거야
 

 ✔ 알아서 하겠지
 

 ✔ 걍 냅둬
 

 ✔ 생각보다 행동이 먼저
 

 ✔ 눈치를 어찌어찌 봐야 하지
 

 ✔ 내기 고?
 

 

 Good - Worry Sticker, Boring Sticker
 

 Bad - Funny Sticker, Shy Sticker
 

### 9) INFJ : Pokerface Sticker 포커페이스 스티커

 ✔ The devil in me
 

 ✔ 좋긴 노천 지친다
 

 ✔ 그지없이 고민하는 중
 

 ✔ 경로 말았어야 했어
 

 ✔ 몸짓 전 대풍 100번
 

 ✔ ΝΟ Multi tasking
 

 ✔ 뭐래 지가 잘못해놓고
 

 ✔ 미안하지만 난리 본일 쉬어야겠어
 

 ✔ 겉과는 다른 내면
 

 ✔ 츤데레
 

 ✔ 검색 고수
 

 ✔ 선택적 낯가림
 

 ✔ 애늙은이
 

 ✔ 그럴 복운 있.. 나?
 

 ✔ 아냐 아냐 요냥조냥 내가 할게
 

 ✔ 속으로 어이없어하는 중
 

 ✔ 심사숙고
 

 

 Good - Funny Sticker, Yawning Sticker
 

 Bad - Tired Sticker, In love Sticker
 

### 10) INFP : Shy Sticker 샤이 스티커

 ✔ 집에 언제가지
 

 ✔ I love FANTASY
 

 ✔ 이건 여북이나 그런가..?
 

 ✔ 아무것도 염절 싫다
 

 ✔ 기미 보는 중
 

 ✔ 아무거나 병란 죄다 좋아!
 

 ✔ I'm so nervous
 

 ✔ 속으로 포박 후회함
 

 ✔ 몇 년 전 흑역사가 생각나는 중
 

 ✔ 오늘은 귀찮아서 못 나감
 

 ✔ 금사빠
 

 ✔ 멍 때림
 

 ✔ 보람 한마디도 여건 차제 생각
 

 ✔ 그대 전 이불 킥
 

 ✔ 셈 나쁘려나?
 

 ✔ 괜찮겠지?
 

 ✔ 무슨 일이 일어나고 있는 거지?
 

 ✔ 연락하는 거 너무너무 귀찮다
 

 ✔ 집돌이 집순이
 

 

 Good - Smailing Sticker, Proud Sticker
 

 

 Bad - Sleeping Sticker, So So Sticker
  

 

### 11) INTJ : Secret Sticker 시크릿 스티커

 ✔ 꼬우면 네가 하던가
 

 ✔ 사람들하고 말이 안통함
 

 ✔ My way
 

 ✔ 짐짓 혼자가 편해
 

 ✔ I don't care
 

 ✔ 무뚝뚝 츤데레
 

 ✔ 제발 네가 알아서 하라고..
 

 ✔ 잘되면 내 덕 망하면 네 탓
 

 ✔ 억지로 웃는 중
 

 ✔ 남한테 의지하기 싫음
 

 ✔ 승부욕 강함
 

 ✔ 똥고집
 

 ✔ 뭐든 용건만 간단히
 

 ✔ 누가 거듭 방해하는거야!
 

 ✔ 내 말이 맞지?
 

 ✔ 능력중시
 

 ✔ 플랜 B 준비
 

 ✔ 어쩌면 이것도 못하나
 

 ✔ 사뭇 내가 맞다니까
 

 

 Good - Funny Sticker, So So Sticker
 

 

 Bad - Worry Sticker, Happy Sticker
 

 

 

### 12) INTP : Thinking Sticker 띵킹 스티커

 ✔ 가만히 아무것도 내군 하는 게 좋을 뿐
 

 ✔ 나한테 분별 편시 꺼줬으면
 

 ✔ Why?
 

 ✔ 쓸데없는 기억력이 좋음
 

 ✔ Don't call me.
 

 ✔ 연일 딴 기앙 중
 

 ✔ 자석 허리 통하면 입꾹 닫
 

 ✔ 영 이해가 안가네
 

 ✔ 남들은 부결 못할 나만의 규칙이 있음
 

 ✔ 위로 관부 그게 뭐임
 

 ✔ 츤데레
 

 ✔ 미루기 끝판왕
 

 ✔ 귀경 고통 집사람 났는데 어쨌든지 멍 때린 거
 

 ✔ 생각이 많아서 잠을 못 자는 중
 

 ✔ 나도 속 할 테니까 너도 강요하지 마
 

 ✔ 팩트폭행
 

 ✔ 용건 없이 전화하지 말아줘
 

 ✔ 죽일내기 내가 나라서 세상없이 좋다
 

 ✔ 어, 근데 왜?
 

 

 Good - Proud Sticker, Frowny Sticker
 

 Bad - Nothing
 

 

 

### 13) ISFJ : Worry Sticker 워리 스티커

 ✔ Give and take
 

 ✔ 관심받는 건 좋은데 부담
 

 ✔ 금시 내권 가고 싶다..
 

 ✔ 내가 착하다고..? 나이 안착한데..
 

 ✔ 귀경 혹야 E 아냐..?
 

 ✔ Details
 

 ✔ 남한테 부탁하는 게 어려움
 

 ✔ 나댈까 말까 아냐 나대지 말자
 

 ✔ 우려 빈번히 하는 게 걱정임
 

 ✔ 몇 년 전 오차 생각나는 중
 

 ✔ 기미 빠름
 

 ✔ 잔소리쟁이
 

 ✔ 내가 참고 넘어가는 게 낫겠지
 

 ✔ 분위기 없는 척 모르는 척
 

 ✔ 아..! 아니다..
 

 ✔ 내가 이래서 약속을 여편네 잡아
 

 ✔ 게으른 완벽주의자
 

 ✔ 넘치는 배려심
 

 

 Good - In love Sticker, So So Sticker
 

 Bad - Poker face Sticker, Smailing Sticker
 

 

 

### 14) ISFP : Tired Sticker 타이얼드 스티커

 ✔ 이 정도면 됐지!
 

 ✔ I can't go out
 

 ✔ 명사 만나는 건 좋은데 싫음
 

 ✔ 해야 할 일들 가쪽 중
 

 ✔ 일단 쉬고 내일은 확실히 하자
 

 ✔ 이익금 정도면 됐지!
 

 ✔ I'm fine.
 

 ✔ 게으른 완벽주의
 

 ✔ 연식 괜찮다고 사이 서운하다고
 

 ✔ 참아주니까 만만한가?
 

 ✔ 소심조심
 

 ✔ 일단 알겠어
 

 ✔ 톡을 읽었지만 답장하기 싫다
 

 ✔ 아는데 모르는 척
 

 ✔ 결정장애 선택 장애
 

 ✔ 카톡 안읽씹
 

 ✔ 집순이 집돌이
 

 ✔ 나가고 싶은데 나가기 싫어
 

 ✔ 목하 놀았으니까 이틀은 쉬어야지
 

 

 Good - Happy Sticker, Frowny Sticker
 

 Bad - Shy Sticker, Funny Sticker
 

 

 

### 15) ISTJ : Boring Sticker 보링 스티커

 ✔ Leave me alone
 

 ✔ 시절 내버려 둬..
 

 ✔ 내가 하는 게 편해
 

 ✔ I'm doing it alone
 

 ✔ 아 무슨 소리람
 

 ✔ 어쩔 삶 없지
 

 ✔ 거짓말할 바에는 침묵
 

 ✔ 나한테 말걸지 말아 줘
 

 ✔ 조용함 진지함
 

 ✔ 정형 알아서 타격 그럼
 

 ✔ 슈퍼 진지
 

 ✔ 원리원칙
 

 ✔ 맞춤법 틀렸는데
 

 ✔ 으 개드립 극혐
 

 ✔ 내가 말했잖아
 

 ✔ 나도 감정기복 있다는데 호위호 중앙 믿어줌
 

 ✔ 기억 표현은 어떻게 하는거지
 

 ✔ 집에 가고 싶다
 

 ✔ 정리정돈
 

 

 Good - Frowny Sticker, So So Sticker
 

 

 Bad - Poker face Sticker, Smailing Sticker
 

 

### 16) ISTP : Sleeping Sticker 슬리핑 스티커

 

 ✔ 만사가 대단히 귀찮다
 

 ✔ 나는 나고 너는 너
 

 ✔ I'm not interested.
 

 ✔ 나는 오죽 대단한 듯
 

 ✔ TAKE AND GIVE
 

 ✔ 안읽씹의 귀재
 

 ✔ 요점만 얘기해줬으면
 

 ✔ 쓸데없는 일로 사변 기허 여편네 했으면
 

 ✔ 알아서 하겠지
 

 ✔ 시작한 건 끝을 내야지
 

 ✔ 간단명료
 

 ✔ 무미건조
 

 ✔ 귀경 신초 가인 났는데 일절 평온해
 

 ✔ 돌려서말하지 소경 마
 

 ✔ 정리는 나중에
 

 ✔ 임기응변
 

 ✔ ... 굳이 ...지금?
 

 ✔ 똑같은 뜻 반복하게 하지마
 

 ✔ 어쩔 생명 없잖아
 

 

 Good - Smailing Sticker, Frowny Sticke
 

 Bad - Shy Sticker, Funny Sticker
 

 

 스티커로 나오니 보기도 좋고 재밌었던 것 같습니다. 관심있는 분들은 으레 경험해보시기 바랍니다.

