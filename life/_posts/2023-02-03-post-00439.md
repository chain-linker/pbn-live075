---
layout: post
title: "우리 아이들의 살은 키로 가진 않는다?"
toc: true
---

 보통 아이들은 성인들보다 면역력과 체력이 약해 병마에 대한 저항력이 떨어진다고 이야기한다. 이는 의학계에 종사하는 사람들 외에도 거의 부모들이 동의하는, 몹시 보편적인 명제이다. 그러니까 어른들은 아이들의 질병이나 사고에 대해 성인의 그것들보다 더 민감하게 반응하는 경우가 많다. 아이들만 전문적으로 치료하는 ‘소아과’가 대단히 하나의 독립적인 의료분야로서 존재하는 것이 정형 방증 아니겠는가(물론 근본적으로 성인의학과 아동의학이 다르기 때문이겠지만). 그렇기는 해도 어른들에게도 위험한 ‘비만’의 경우에는 이상하리만큼 별다른 관심을 쏟지 않는다. 진정 이상한 현상이다.
 

 부유함(풍요로움)과 비만 사이에는 뒤미처 확립된 상관관계가 있으며, 더 부유한 사회에서 가끔가다 일층 높은 비만율을 볼 성명 있다. 이러한 상관관계는 미국, 캐나다, 게다가 몇몇 유럽 국가들을 포함한 세계의 많은 나라에서 관찰되어 왔다.
이 상관관계에 기여할 요행 있는 몇 말초 요인이 있습니다. 첫째로, 한층 부유한 사회의 사람들은 더 다양한 고칼로리, 가공식품, 그리고 편리한 음식에 접근하는 경향이 있다. 이러한 음식들은 가지각색 설탕, 지방, 더욱이 소금이 많고, 체중 증가와 비만과 관련이 있습니다.
더 부유한 사회의 사람들은 앉아서 일하는 직업과 생활방식의 보급으로 인해 직장에 가기 위해 걷거나 자전거를 타는 것과 같은 신체 활동의 기회를 한층 적게 종자 수로 있다. 이러한 신체 활동의 부족은 체중 증가와 비만에 기여할 수명 있다.
더 부유한 사회의 사람들은 더욱이 과식과 체중 증가에 기여할 핵 있는 보다 큰 수준의 스트레스를 가질 삶 있다.
이러한 풍요로움과 비만의 상관관계가 절대적인 것은 아니며, 유전학, 개인의 행동, 문화적 요인을 포함하여 비만의 원인이 될 생령 있는 다른 많은 요인들이 있다는 것을 주목하는 것이 중요하다. 그러나, 부유함과 비만의 관계를 이해하는 것은 비만의 만연과 관련된 건강 문제를 줄이기 위한 민중 보건 노력에 유용할 행복 있다.
 

 일단 이런 이상한 현상의 이유를 차치하고서라도, 이미 아동비만은 우리가 관심을 기울여야 할 만큼 사회적인 문제가 되었다. 전 세계적으로 아동비만은 조금씩 흔한 일이 되어가고 있는 추세다. 이것은 그만치 책임소재가 복잡한 문제이다. 실지로 방송에 출연한 누구 의사는 ‘성인의 비만은 본인 책임이지만, 아동비만은 전적으로 부모의 책임’이라고까지 말한 상당히 있다.
 서울특별시 학교보건진흥원의 자료(최근 18년간 비만아 증가 양상)에 따르면 근래 18년간 초등학교 남학생의 정황 6.4배(1979년 3.6%에서 1996년 23.0%), 여학생의 예시 4.7배(1979년 3.3%에서 1996년 15.5%), 중고등학교 남학생의 호소 3.0배(1979년 5.2%에서 1996년 15.4%), 여학생의 사연 2.4배(1979년 6.3%에서 1996년 15.0%)로 증가한 것으로 나타났다.
 자전 데이터가 말해주는 것은 다음과 같다: “여자보다 남자가, 아울러 중고등학교 연령층보다 초등학교 연령층이 우극 높은 비만 증가세를 보인다. 또 비만아 중에서도 표준체중보다 50%이상 체중이 한층 나가는 고도 비만아의 비율이 증가하고 있다.”
 오히려 대부분의 사람들은 실리 사항을 별로 심각하게 받아들이지 않을 것이다. 그럼에도 소아비만은 성인의 비만과 동일하게 고지혈증, 지방간, 고혈압과 당뇨병 같은 성인병 증상들을 수반한다. 특히 고도 비만군(비만도 150%이상)에서는 고지혈증(61%), 지방간(38%), 고혈압(7%), 당뇨병(0.3%) 등 78%이상이 합병증 속 임계 원려 합병증을 가지고 있고, 통계적으로도 절반 이상이 성인비만으로 발전되므로 가군 장기적인 논제 중의 하나라고 볼 복수 있다. 왜냐고? 어릴적 병마가 아이에게 큰 영향을 남기듯, 아동비만 역시도 아이의 인생을 통틀어서 굵직한 흔적을 남길 가능성이 적잖이 높다. 그리하여 신체적 성장이 완료된 상황인 성인보다 성장이 비약 중인(그리고 네년 상태로 성장하는 관성을 고려했을 때) 유소년의 비만이 장기적으로 한층 큰 잔재를 남기기 때문이다. 아이가 시간이 지나 성인이 되어감에 따라 ‘아동비만도 성인비만이 되는’ 그런 간단한 차원의 문제가 아니다.
 성인이 되어 살이 찌는 것은 단순히 지방세포의 크기가 커지는 것이기 그러니까 다이어트 후 그 크기가 기존대로 돌아온다. 그러나 아동비만의 작용기전은 다과 퍽 다르다.
 아동비만은 지방세포가 증식해 그쪽 숫자가 많아진다는 것이 기존 학계의 정설이었다. 도리어 최근의 연구에 의하면 신체 조직에 따라 지괴 세포수 위주로 증가하는 경향을 보일 때도 있고, 세포 부피가 커지는 경향을 보일 때도 있다고 전해졌다.
 이에 따르면 비만상태로 자란 아이의 몸은 ‘이상 상태’의 대지 세포의 비율을 ‘정상 상태’로 인식하게 된다. 간단하게 말해서 아이의 몸에 각인된 ‘평소에 지방세포가 이 정도는 있어야 해’라는 비율이 전반 사람보다 더욱 높다는 것이다.
 역시 장기적으로 축적된 지방이 셀룰라이트화(지방조직이 무제한으로 증식 및 팽창해 피부로 부풀어 오른 상태)하고, 신진대사 계단 및 골격이 비만 체형에 맞게 일그러지고 왜곡되는 경우가 많아 성인비만보다 훨씬 탈출이 힘들다.
 

 비만이 깨뜨리는 신체의 균형은 호르몬까지도 포함되는데, 이는 신체적 문제에서 내적 문제까지도 망라할 만큼 문제의 범위가 넓고 심각하다.
 호르몬은 신체작용뿐 아니라 감정적 작용까지 조절하는 복잡한 물질이다. 인체에 작용하는 수많은 호르몬은 아직까지 과학적으로도 충족히 규명되지 않은 경우가 대부분이고, 바꿔 말해서 호르몬 교란의 영향은 쉽게 가늠하기 힘든 결과를 낳는다는 것이다. 그렇다, 비만은 아까 이런 엄청난 부분까지 건드린다. 아동은 육체뿐 아니라 정서적 측면에서도 성인보다 취약하다. 형씨 말인즉 어려서부터 경험하는 호르몬 교란은 아동에게 상당한(경우에 따라서 ‘치명적’이라고 묘사할 한가운데 있는) 정신적인 영향을 남기게 된다고 볼 길운 있다.
 과학적으로 ‘확실하게 규명된 부분’만 언급해보자면, 아동비만으로 인한 내분비장애, 성조숙증, 고혈압, 당뇨 같은 가지가지 질환에 걸릴 위험이 무지무지 높아지며, 외모에 민감한 청소년기의 자존감에 큰 상처를 남겨 평생의 트라우마가 될 확률도 높다. 엄밀히 말해 완전히 특별한 경우가 아닌 첨단 성인이 되어서도 한동안은 외모로 인한 스트레스가 필연 존재할 것이다. 특히나 한국처럼 사회적인 시선을 생심 쓰는 사회에서 그쪽 의미는 말해 뭐 하겠는가, 여러모로 애석한 일이다.
 흔히들 어른들이 통통한 체형의 아이를 위로하는 말로 ‘크면 살이 모두 키로 가니깐 근심거리 말아라’고들 한다. 그럼에도 애석하게도 이금 흔하디흔한 말은 아주 심각한 오류를 가지고 있다. 살은 절대 키로 갈래 않는다, 오히려 네놈 반대이다.
 성장호르몬은 크게 두 말초 작용을 한다: (1) 신체 조직을 성장시키며, (2) 지방을 분해한다. 새삼 말해서 어른들이 말하는 ‘살이 키로 간다’는 것은 성장호르몬이 폭발적으로 생성되는 시기가 오면 키가 자라는 동시에 지방이 분해되므로 ‘살이 키로 가는 것처럼’ 보이는 것이다.
 자전 말인즉, 아동비만의 애걸 아이의 성장호르몬이 지방을 분해하는데 낭비되어서 키가 우극 못 크는 너무 유감스러운 사태가 벌어질 한가운데 있다는 소리다.
 천만다행으로 아이가 성장호르몬이 무척 충분하게 나와서 키는 소경 컸다고 가정해보자(그런 경우도 드물지 않게 있으므로). 그렇지만 문제는 내내 남아있다.
 첩경 오래된 지방은 셀룰라이트화하면서 장기적으로 축적된다. 특별히 가슴과 허리 밑부분에 축적된 지방들은 여유증과 ‘함몰음경’(허리 아래, 바로 치골 부위에 [korea male breast](https://scarfdraconian.com/life/post-00037.html) 살이 쪄서 성기가 파묻히는 증세)이 되는 경우가 있다. 골격이 과도한 체중에 적응하여 기형적인 구조를 가지는 것도 빼놓을 생령 없다.
 상술한 경우의 문제를 가지고 있는 아동(또는 정형 아이들이 어느 정도 자란 이후의 청소년)들이 다이어트를 하면 어느 수평기 완화되는 케이스도 있으나, 인대같이 민감한 조직에 문제가 생긴 경우는 회복이 힘들다.
 

 인간은 수십만 년간, 어쩌면 인제 역사로 기록된 수천 년 전까지 ‘풍요로운 삶’과는 거리가 멀었다. 살기 위해 투쟁해야 했고, 먹기 위해 땀을 흘려야 했다. ‘평범한 사람’들이 손만 뻗으면 먹을 것을 구할 삶 있게 된지는 상금 100년이 되지 않았다. 시방 우리나라만 해도 상금 ‘보릿고개’라는 개념을 기억하는 세대가 살아있지 않은가.
 비만은 ‘풍요로움의 역습’이다. 기외 에너지라는 축복을 얼마 누리지도 못한 인류가 겪고 있는 시련이다. 설혹 애초에 풍요롭지 못한 삶을 살게 설계된 인간을 질투하는 자연의 심술, 그것이 제대로 비만이 아닐까 하는 생각이 든다.
 

