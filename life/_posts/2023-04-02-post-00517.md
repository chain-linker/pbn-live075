---
layout: post
title: "[625] 미스코리아-총정리(20190203)-버전2"
toc: true
---



 ['차단용 토끼'의 녹음기록 파일형식 117]-(20220530)-[비밀결사체들과 재벌카르텔의 '비밀스러운 여성들 명단']
 -----------------------------------------------------------------------------------------------------------------------------------------------------------
 

 

 

 

 

 

 

 -----------------------------------------------------------------------------------------------------------------------------------------------------------
 (기록: 2019.02.03/일요일/PM 09:26)
 

 [미스코리아]
 설명: 한국일보가 주최하는 대한민국의 미인대회로 1957년부터 시작했다. 학력(고등학교 졸업 이상), 연령(한국나이 19~27세), 기혼 유무(미혼) 등의 말단 요소가 있으며 본선에 진출하기 위해서는 미스코리아 밑면 대회에서 입상해야 한다.
 [알림: 연관 '미스코리아 명단'은 별도의 작위목록에 기입을 다리파 않는다.]
[예외1: 애오라지 '여배우-모델' 항목에는 명단을 중복 기입한다.
============================================================================
[기사 작위]
 강정현(KANG JEONG HYEON)/2018년 미스코리아 후보자
 지역: 광주전남
나이: 22세
출신: 정보호남대학교 항공서비스학과 4학년 재학
장래희망: 승무원
취미/특기: 영화감상 , 특기 - 요리
신체정보: 174cm/54.7kg/36 - 25 - 35
 [남작 작위]
 김계령/2018년 미스코리아 미/2018년 미스코리아 인천 진
 나이: 22세
학력: 서강대학교 경영학과
장래희망: 아나운서, 재무분석가
취미/특기: 발레, 첼로 연주
신체정보: 172.4cm/50.8kg
경력: 2018년 미스코리아 미
2018년 미스코리아 인천 진(우승)
 [준남작 작위]
 김규리/2015년 미스 경남 진
 나이: 26세
경력: 2015년 에러 경남 진
 [기사 작위]
 김규리(1996년)/2018년 미스코리아 부산-울산 선
 출생: 1996년 1월 9일, 부산광역시
학력: 부산대학교 심리학과
신체: 177.4cm/54kg/34-24-35
경력: 2018년 미스코리아 부산-울산 선
2018년 미스코리아 2차 통과자
 [기사 작위]
 김규리/2017년 미스코리아 추축 선
 소속: 에스팀 소속 패션 모델
경력: 2017년 미스코리아 경합 선
 [남작 작위]
 김나영(1989년)/2014년 미스코리아 USA 미(참가번호 32번)
 설명: 2014 미스코리아 USA 미. USA 대회에서는 한국일보에 입상했으나 본선에서는 미로 출전하였다
 참가번호: 32
생년월일: 1989년 9월 25일
신체사항: 167.3cm, 48.2kg, 34-24-34
학교: University of Alberta 경제학과
취미: 음악감상, 마라톤
특기: 태권도, 하키
장래희망: UN 근무 및 봉사
경력: 2014년 미스코리아 USA 미(참가번호 32번)
 [준남작 작위]
 김나영/2018년 미스코리아 경북 미
 나이: 24세
신체: 175.1cm/56.9kg
학력: 대경대학교 모델과 2학년
특기: 필라테스
장래: 희망모델
경력: 2018년 미스코리아 경북 미
 [준남작 작위]
 김도연/2014년 미스코리아 경기 미
 참가번호: 5
생년월일: 1990년 5월11일
신체사항: 172.2cm 53.1kg 34-23-36
학교: 배화여자대학교 중국어통번역과
취미: 요리, 영화감상
특기: 수영, 요가
장래희망: 방송인
 [기사 작위]
 김려은(KIM RYEO EUN)/2017년 2차 통과자
 지역: 대구
나이: 만 27세
신체: 172.6cm / 50kg / 34-24-36
출신: 정보경북대학교 대학원
전공: 신문방송학과
취미: 헬스 / 사진찍기
특기: 캘리그라피 / 포토포즈
 [준남작 작위]
 김미정/2018년 미스코리아 경북 참가(참가번호 11번)
 나이: 20
학력: 연성대학교 항공서비스학과 2학년
특기: 이미지컨설팅, 태권도
장래: 희망뉴스앵커
경력: 2018년 미스코리아 경북 참가(참가번호 11번)
 [기사 작위]
 김별이(BYEOLI KIM)/2018년 2차 미스코리아 통과자
 지역: 충북
나이: 23
학력: 광주여자대학교 항공서비스학과
장래희망: 교수, 장학재단 설립자
취미/특기: 독서, 홈트레이닝, 요거트 만들기/음악감상/요가, 댄스
신체정보: 174.7cm / 51.2kg
 [기사 작위]
 김서원/2018년 미스코리아 대구 선
 나이: 22
학력: 백석예술대학교 항공서비스과 졸업
특기: 연기,다양한 스포츠,무용
장래희망: 모델겸 배우
경력: 2018년 미스코리아 대구 선
 [준자작 작위]
 김수민(1994년)/한국의 모델/2018년 미스코리아 진
 설명: KBS 2TV 해피투게더 8월 2일 방송분에 출연하였고 같은 타격 10월에는 KBS 1TV 6시 내고향에서 강남과 나란히 출연하였다.
 출생: 1994년 10월 1일 (24세)
학력: 디킨슨 단과대학 국제경영학과
신체: 173.4cm, 58.9kg, 35-25-38
경력: 2018년 미스코리아 진(우승)
 [기사 작위]
 김수현(KIM SU HYUN)/2018년 미스코리아 후보자
 지역: 서울
나이: 22세
출신정보: 홍익대학교 법학과(공법전공) 3학년 휴학
장래희망: 판사
취미/특기: 미술관 관람, 미술작품감상, 여행, 요가, 필라테스
특기: 수학, 스키, 와이어공예, 요가, 필라테스
신체정보: 172.9cm/50.8kg/34 - 24 - 35
 [준남작 작위]
 김소은(KIM SO EUN)/2017년 미스코리아 2차 통과자
 지역: 경남
나이: 만 24세
신체: 170.5cm / 49.1kg / 35-24-36
출신: 부산대학교
전공: 한국음악학과
취미: 스쿠버다이빙 / 여행
특기: 대금연주 / 장구춤
장래희망: 한국음악학과 교수
경력: 2017년 미스코리아 2차 통과자
 [기사 작위]
 김영은/2018년 미스코리아 경북 참가(참가번호 10번)
 나이: 24
학력: 인하대학교 예술체육학부 4학년
특기: 무대디자인, 동화구연, 수영
장래희망: 문화예술교육사
경력: 2018년 미스코리아 경북 참가(참가번호 10번)
 [남작 작위]
 김정진(1994년)/2015년 미스코리아 선
 출생: 1994년 5월 30일, 충청북도 청주시
국적: 대한민국
학력: 공주대학교 화학과
신체: 171.5cm, 56.9kg, B형
성좌/지지: 쌍둥이자리/개띠
경력: 2015년 미스코리아 선
2015년 건우 충북-세종 당선자
 [준남작 작위]
 김태영(KIM TAE YEONG)/2017년 미스코리아 2차 통과자
 지역: 부산울산
나이: 만 24세
신체: 173.9cm / 53.8kg / 34-23-36
출신: 정보부산대학교 의류학과
취미: 풀루트 / 발레
특기: 뮤지컬 / 영어
장래희망: 패션디자이너
경력: 2017년 미스코리아 2차 통과자
 [준남작 작위]
 김한솔(KIM HANSOL)/2018년 미스코리아 대구 참가(참가번호 15번)
 나이: 23
학력: 계명대학교 미국학전공 3
특기: 풍경화, 운동
장래희망: 항공승무원
경력: 2018년 미스코리아 대구 참가(참가번호 15번)
 [준남작 작위]
 김현희/2014년 미스코리아 대구 진
 오차 코리아 참가번호: 8
생년월일: 1990년 8월 2일
신체사항: 169.8cm, 46.9kg, 33-24-36
학력: 경북대학교 시각정보디자인학과
이화여자대학교 대학원 시각디자인과
 취미: 한국무용, 필라테스
특기: 디자인, 리폼
장래희망: 문화예술 CEO
경력: 2014년 미스코리아 대구 진
 [준남작 작위]
 구윤희/2018년 미스코리아 경북 참가(참가번호 06번)
 나이: 23
학력: 대구대학교 유아특수교육과 졸업
특기: 요리
장래: 희망교사
경력: 2018년 미스코리아 경북 참가(참가번호 06번)
 [준남작 작위]
 박수진(PARK SUJIN)/2018년 미스코리아 대구 참가(참가번호 19번)
 나이: 24
학력: 광주여자대학교 항공서비스학과 졸업
특기: 수영
장래희망: 항행 승무원
경력: 2018년 미스코리아 대구 참가(참가번호 19번)
 [남작 작위]
 박아름/2015년 미스코리아 미
 설명: 2015 미스코리아 미. 2015 미스 인터내셔널에 대한민국 대표로 출전해 아시아상을 수상했다.
 출생: 1991년 5월 9일
학력: 아이오와 분과대학 심리학
원화여자고등학교
 신체: 168cm, 48.9Kg, 34-24-36, B형
성좌/지지: 황소자리/양띠
경력: 2015년 실례 대구 진
2015년 미스코리아 미
2015년 오류 인터내셔널 아시아상
 [기사 작위]
 백지현/2014년 미스코리아 미
 생년월일: 1993년 3월 27일
미스코리아 참가번호: 28
신체사항: 175.4cm, 49.1kg, 33-24-36
학교: 계명대학교 성악과
취미: 오페라감상, 피겨스케이팅 감상
특기: 성악, 수영
장래희망: 음악치료사
경력: 2014년 부정 대구 미
2014년 미스코리아 미
2018년 병 유니버스 코리아
 [기사 작위]
 신수진(SHIN SUJIN)/2018년 미스코리아 대구 참가(참가번호 17번)
 나이: 26
학력: 성신여자대학교 미디어영상연기학과 졸업
특기: 승마, 요리
장래희망: 콘텐츠 크리에이터
경력: 2018년 미스코리아 대구 참가(참가번호 17번)
 [기사 작위]
 신혜빈(SHIN HYE BIN)/2017년 미스코리아 2차 통과자
 지역: 인천
나이: 만 23세
신체: 171.7cm / 47.4kg / 33-22-36
출신: 정보인하공업전문대학
전공: 항공운항과
취미: 객려 / 사진촬영
특기: 준이 / 요리
장래희망: 항공 객실승무원
 [남작 작위]
 서예진(1997년)/2018년 미스코리아 선/2018년 미스코리아 중심 진
 설명: 쥬얼리 디자이너를 희망하며 이화여자대학교 동양화과에 입학했다.
 출생: 1997년, 서울특별시
학력: 이화여자대학교 동양화과
신체: 172cm, 51kg, 34-25-36
경력: 2018년 미스코리아 선
2018년 미스코리아 중앙 진(우승)
 [기사 작위]
 서재원(SEO JAE WON)/2017년 미스코리아 진
 지역: 경기
나이: 만 23세
신체: 175.2cm/54kg/35-24-36
학력: 한국예술종합학교
전공: 한국무용
취미: 서핑
특기: 한국무용 / 발레
장래희망: 무용가
경력: 2017년 미스코리아 진
 [준남작 작위]
 손정은(1994년생)/2018년 미스코리아 대구 미
 출생: 1994년생/대구광역시 출신.
신체: 170.9cm / 52.9kg
학력: 대구교육대학교 초등미술교육과 재학 중
특기: 필라테스, 요리
장래희망: 초등학교 은사 , 아트디렉터
경력: 2018년 미스코리아 대구 미
2018년 미스코리아 1차 통과자
 [남작 작위]
 손소희/2015년 미스코리아 경남 미
 지역: 대한민국 창원
나이: 26세
신체: 178cm
학력: 부산외대 영어학부
경력: (2017년) 맥스큐 머슬마니아 오리엔트 챔피언십'에선 미즈 비키니 구간 1위
(2016년 9월)머슬마니아 법회 모델 한계 3위
(2016년 9월)머슬마니아 회합 모해 미즈 비키니 톨부문 5위
(2015년)미스코리아 경남 미
 [준남작 작위]
 손희주(1996년)/2018년 미스코리아 부산-울산 진
 출생: 1996년 11월 12일, 부산광역시
학력: 부산대학교 광메카트로닉스공학과
신체: 168cm/47.2kg/33-21-33
경력: 2018년 미스코리아 부산-울산 진
2018년 미스코리아 2차 통과자
 [준자작 작위]
 송수현(1992년)/2018년 미스코리아 선/2018년 미스코리아 대구 진
 설명: 2018 오차 대구 진 당선자로, 2018 미스코리아 선이기도 하다.
 출생: 1992년 9월 6일, 대구광역시
학력: 동덕여자대학교 방송연예과
신체: 175.5cm, 55.2kg, 34-24-37
경력: 2018년 미스코리아 선
2018년 미스코리아 대구 진(우승)
2018년 실족 어스 코리아
 [기사 작위]
 안현영(HYEON YEONG AN)/2018년 미스코리아 2차 통과자
 지역: 인천
나이: 20
학력: 인하공업전문대학 항공운항과
취미/특기: 스쿠버다이빙, 음악감상, 커피만들기
장래희망: 브랜드창업, CEO
신체정보: 172.8cm/52.5kg
경력: 2018년 미스코리아 2차 통과자
 [준자작 작위]
 이서빈/2014년 미스코리아 선/아나운서
 출생: 1993년 4월 29일, 경기도 의왕시 대한민국
학력: 한국외국어대학교 태국어과
소속: 써브라임아티스트에이전시
신체: 176.1cm, 56.8kg, 32-26-36
취미: 영화감상, 요가
특기: 태국어, 그림그리기
수상: 2014 미스코리아 스포츠 미
2014 미스코리아 선
 경력: 한국음반산업협회 홍보대사 (2014)
초록우산 어린이재단 홍보대사 (2014)
의왕시 홍보대사 (2014)
K STAR 라이브 스타뉴스 진척 (2016~현재)
 [준남작 작위]
 이수연(LEE SOO YEON)/2017년 미스코리아 미
 지역: 경북
나이: 만 24세
신체: 169.3cm / 51.8kg / 35-24-36
출신정보: 성신여자대학교
전공: 미디어영상연기
취미: 인테리어 / 등산
특기: 사격 / 날씨소개
 [준남작 작위]
 이수희(LEE SUHEE)/미스 대구 참가(참가번호 03번)
 나이: 23
학력: 대구대학교 미디어커뮤니케이션과 졸업
특기: 프레젠테이션, 운동
장래: 희망광고기획자
경력: 비 대구 참가(참가번호 03번)
 [기사 작위]
 이승아(LEE SEUNGAH)/미스 대구 참가(참가번호 04번)
 나이: 25
학력: 숙명여자대학교 성악과 4학년
특기: 성악, 골프
장래: 희망성악가
경력: 에러 대구 참가(참가번호 04번)
 [준남작 작위]
 이소헌(SO HEUN LEE)/2018년 미스코리아 2차 통과자
 지역: 경남
나이: 22
학력: 광주여자대학교 항공서비스학과
장래희망: 승무원
취미/특기: 애견미용, 등산/양궁
신체정보: 172.8cm/46.8kg
 [준남작 작위]
 이영리(LEE YEONGRI)/미스 대구 참가(참가번호 02번)
 나이: 23
학력: 수성대학교 뷰티스타일리스트과 졸업
특기: 헬스,필라테스,메이크오버
경력: 양 대구 참가(참가번호 02번)
 [준남작 작위]
 이지현/2018년 오류 경북 참가(참가번호 09번)
 나이: 24
학력: 경북대학교 대학원 신문방송학과 1학년
특기: 피아노
장래: 희망방송인
경력: 2018년 실수 경북 참가(참가번호 09번)
 [남작 작위]
 이정은(LEE JUNG-EUN)/2018년 [홍대 필라테스](https://rubhope.com/life/post-00043.html){:rel="nofollow"} 부정 대구(참가번호 06번)
 나이: 23
학력: 계명대학교 통계학과 4학년
특기: 댄스, 운동
장래: 희망승무원
경력: 2018년 비 대구(참가번호 06번)
 [준남작 작위]
 이한나(LEE HANNA)/2017년 미스코리아 선
 지역: 필리핀
나이: 만 20세
신체: 171.8cm / 56.1kg / 35-25-36
출신: Lyceum of the Philippines University
전공: Foreign Service
취미: YouTube 동영상시청 / 독서
특기: 언어배우기 / 암기
장래희망: NGO봉사자
 [준남작 작위]
 이혜인(LEE HYEIN)/2018년 오류 대구(참가번호 09번)
 나이: 20
학력: 대구가톨릭대학교 스페인어중남미학부 1
특기: 댄스
경력: 2018년 실족 대구(참가번호 09번)
 이효림(HYO RIM LEE)/2018년 1차 통과자
 지역: 강원
나이: 22세
학력: 가천대학교 회화과
취미/특기: 운동 / 미술, 피아노
장래희망: 규범 겸 디자이너
신체정보: 167.5cm/49.8kg
 [자작 작위]
 임경민(1997년)/2018년 미스코리아 미/2018 그릇 경북 진
 설명: 대구에서 태어나 대구에서 자라왔으며 어렸을 적부터 소아 모델로 활동했다. 초등학교 6학년 때부터 기위 키 168cm를 넘어, 현재는 178cm의 장신에 이르게 된다. 큰 키로 인해 어려서부터 여러 운동부 감독과 코치들에게 운동을 하라는 권유를 받았다. 길에서의 모범 제의도 잦았다고 한다
 출생: 1997년 11월 22일, 대구광역시
학력: 대구가톨릭대학교 수학교육과
신체: 178cm, 53kg, 33-24-36
경력: 2018년 미스코리아 미
2018년 괘오 경북 진(우승)
 [준남작 작위]
 임주현(LIM JUHYUN)/2018년 실례 대구(참가번호 13번)
 나이: 26
학력: 대구대학교 관광경영학과 4학년
특기: 중국어
장래희망: 프리랜서, 종합무역중개인
 [준남작 작위]
 원다예/2018년 처녀 경북 참가(참가번호 05번)
 나이: 26
학력: 대구대학교 디자인대학원 1학년
특기: 태권도
장래: 희망교수
경력: 2018년 잘못 경북 참가(참가번호 05번)
 [준남작 작위]
 원애경/2018년 미스큐 경북 참가(참가번호 12번)
 나이: 26
학력: 건국대학교 대학원 예술디자인 졸업
특기: 고전머리연출, 달리기
장래희망: 뷰티디자인 전공 교수
경력: 2018년 허물 경북 참가(참가번호 12번)
 [남작 작위]
 육지송(1994년)/2018년 미스코리아 가운데 미
 설명: 미국의 명문 뉴욕주립대학교 빙햄턴 캠퍼스에 입학한 재원으로, 사역활동과 기도회 등 독실한 크리스천으로서의 활동을 이어나가다가 미스코리아에 참가하게 되었다. 참고로 헤어 전담은 파크뷰 칼라빈 헤어의 부원장 배욱진이 맡았다.
 출생: 1994년, 서울특별시
학력: 뉴욕 주립대학교 빙햄튼 영어영문학과
신체: 178cm, 58.5kg, 34-26-36
경력: 2018년 미스코리아 한복판 미
 [준남작 작위]
 전주미(JU MI JEON)/2018년 2차 통과자
 지역: 인천
나이: 19
학력: 인하공업전문대학 항공운항과
장래희망: 여성CEO
취미/특기: 여행, 꽃꽂이 / 피겨스케이트, 승마, 수영
신체정보: 171.9cm/52kg
 [기사 작위]
 정다혜(JUNG DAH HYE)/2017년 미스코리아 선
 지역: 서울
나이: 만 23세
신체: 169.7cm / 45.3kg / 33-23-34
학력: 정보숙명여자대학교
전공: 피아노과
취미: 독서/피아노
특기: 베이킹/피아노
장래희망: 언론인
 [기사 작위]
 정두란(DU RAN JUNG)/2018년 미스코리아 2차 통과자
 지역: 서울
나이: 21
학력: 성신여자대학교 성악과
장래희망: 성악가 겸 방송인
취미/특기: 프랑스자수, 필라테스/성악, 수영
신체정보: 176.7cm/54kg
 [기사 작위]
 정명지(JUNG MYEONG JI)/2017년 미스코리아 2차 통과자
 지역: 경남
나이: 만 24세
신체: 171.7cm / 48.4kg / 34-24-35
출신: 광주여자대학교 연극영화학과
광주여자대학교 항공서비스학과
 취미: 여행스크랩북만들기 / 가구공방
특기: 치어리딩 / 카운셀링
 [준자작 작위]
 정희지(HUI JI JUNG)/2018년 미스코리아 2차 통과자
 지역: 경남
나이: 22
학력: 대경대학교 스킨케어과
장래희망: 뷰티컨설턴트
취미/특기: 글램핑,수영/스쿠버다이빙, 플라워플레이팅
신체정보: 169cm/47.6kg
 [준남작 작위]
 주미소/2018년 와오 경북 선
 나이: 26
학력: 계명대학교 관광경영학과 졸업
특기: 태국요리 만들기
신체: 174.1cm/50.1kg/33-25-34
경력: 2018년 과실 경북 선
 [기사 작위]
 조유정(JO YOUJEONG)/2018년 전비 경북 참가(참가번호 14번)
 나이: 24
학력: 영남대학교 국어국문학과 3
특기: 요리
장래희망: 객실승무원
경력: 2018년 실족 경북 참가(참가번호 14번)
 [준남작 작위]
 조정은(CHO JEONG EUN)/2018년 탓 경북 참가(참가번호 15번)
 나이: 23
신체: 166cm/53.8kg/35-26-36
학력: 성신여자대학교 생명과학화학부 3학년
특기: 수화, 가야금
장래: 희망언론인
경력: 2018년 실족 경북 참가(참가번호 15번)
 [기사 작위]
 조혜민(HYE MIN JO)/2018년 미스코리아 1차 통과자
 지역: 광주전남
나이: 22
학력: 서경대학교 모델연기전공
장래희망: 모델테이너, 연기 곧장 하는 배우
취미/특기: 회소 그리기, 음 부르기 / 워킹, 연기
신체정보: 174.5cm / 53.3kg
 [기사 작위]
 하유진(HA YU JIN)/2017년 미스코리아 2차 통과자
 지역: 부산, 울산
나이: 만 24세
신체: 166.8cm / 50.9kg / 34-24-35
출신: 홍익대학교
전공: 산업디자인학과
취미: 식선 / 여행
특기: 골프 / 미디어아트
 [기사 작위]
 황시아 HWANG SIA/2017년 미스코리아 2차 통과자
 지역: 경남
나이: 만 23세
신체: 169.5cm / 49.6kg / 33-23-36
출신: 계명대학교
전공: 관광경영학
취미: 수영 / 골프
특기: 피아노연주 / 플루트연주
장래: 희망에스테틱 마크 CEO
경력: 2017년 미스코리아 2차 통과자
 [기사 작위]
 홍서영(HONG SEO YOUNG)/2017년 2차 통과자
 지역: 강원
신체: 168.7cm / 48.1kg / 34-24-36
학력: 정보고려대학교
전공: 미디어학부 / 경영학과
취미: 베이스기타연주 / 필라테스
특기: 프레젠테이션 / 영어 통역
장래희: 망다큐멘터리 프로듀서
나이: 만 21세
============================================================================
[작위가 없는 예비부인]
 ※ 아래는 미스코리아 후보 중가운데 작위가 없는 예비부인들이다.
※ '여배우-모델' 명단에 작위가 있을 정세 견련 명단의 작위로 기록한다.
 김명서(MYUNG SEO KIM)/2018년 1차 통과자
 지역: 대전,세종,충남
나이: 23세
학력: 한서대학교 항공관광학과
장래희망: 승무원, 소믈리에
취미/특기: 요리하기, 셀프네일아트, 음악감상
신체정보: 172.8cm / 51.2kg
 김지연(JI YEON KIM)/2018년 1차 통과자
 지역: 경기
나이: 22세
학력: 서일대학교 연극과
장래희망: 매력적인 배우
취미/특기: 연기,볼링,아크로바틱,댄스
신체정보: 169.2cm / 50.5kg
 김지원(KIM JI WEON)/2017년 2차 통과자
 지역: 대구
나이: 만 23세
신체: 171.5cm / 49.5kg / 34-23-36
출신: 계명대학교
전공: 성악과
취미: 연주감상 / 걷기
특기: 음 / 피아노연주
 김하늘(KIM HA NEUL)/2017년 2차 통과자
 지역: 대전,세종,충남
나이: 만 25세
신체: 170.7cm / 53.1kg / 34-24-36
출신: 한서대학교
전공: 항공관광학과
취미: 영화감상 / 손편지쓰기
특기: 요가 / 웨이트트레이닝
 김희란(HEE RAN KIM)/2018년 1차 통과자
 지역: 중국
나이: 22세
학력: 동덕여자대학교 모델과
장래희망: 방송인
취미/특기: 꽃꽂이 / 중국어
신체정보: 171.5cm / 51.3kg
 김희로(HERO KIM)/2018년 1차 통과자
 지역: 서울
나이: 24세
학력: 동덕여자대학교 모델과
장래희망: 크리에이터, 플랫폼사업CEO
취미/특기: 중국어(외국어 배우기) / 요가
신체정보: 171.5cm / 51.2kg
 남승우(NAM SEUNG WOO)/2017년 미스코리아 미
 지역: 서울
나이: 만 25세
신체: 170.7cm / 53.3kg / 34-24-35
출신: 중앙대학교
전공: 경영학부
취미: 내한 / 요리
특기: 메이크업
장래희망: 뷰티저널리스트
 박예하(YE HA PARK)/2018년 1차 통과자
 지역:강원
나이: 21
학력: 동덕여자대학교 모델과
장래희망: 방송인, 작곡가
취미/특기: 피아노연주 / 작사, 작곡
신체정보: 174.9cm / 51.9kg
 신민지(MIN JI SHIN)/2018년 1차 통과자
 지역: 충북
나이: 21세
학력: 연성대학교 항공서비스과
장래희망: 객실승무원
취미/특기: 등정 / 바이올린, 연주
신체정보: 172.5cm / 54.2kg
 신은혜(EUN HYE SHIN)/2018년 1차 통과자
 지역: 부산/울산
나이: 23세
학력: 동아대학교 한국화전공
장래희망: 유명한국화작가, 미술선생님
취미/특기: 수상스키, 서핑, 도자공예, 캘리그라피/풍경화그리기, 캐리커처
신체정보: 173.9cm / 52.2kg
 석유진(SUK YU JIN)/2017년 2차 통과자
 지역: 서울
나이: 만 25세
신체: 172.9cm / 50.9kg / 35-23-37
출신: 한국외국어대학교
전공: 언어인지과학과
취미: 탐승 / 영화관람
특기: 앞뒤로 다리찢기 / 소묘
 이하나(LEE HANA)/2017년 2차 통과자
 지역: 일본
나이: 만 27세
신체: 177.9cm / 57.6kg / 35-25-37
출신: 백제예술대학교 모델과
취미: 싸이클링 / 영화관람
특기: 싸이클링 / 워킹
 윤이지(YI JI YOON)/2018년 1차 통과자
 지역: 전북
나이: 24세
학력: 연성대학교 관광과 호텔관광전공
장래희망: CEO
취미/특기: 노래,요리,춤,운동
신체정보: 176.6cm / 59.8kg
 전은화(EUN HWA JEON)/2018년 1차 통과자
 지역: 경남
나이: 26세
학력: 명지대학교 아동학과
장래희망: 변호사
취미/특기: 판소리, 바이올린, 우쿨렐레, 리포팅, 노래, 춤, 승마, 골프, 등산, 독서
신체정보: 164.8cm / 49.2kg
 조세라(JO SE RA)/2017년 2차 통과자
 지역: 대전세종충남
나이: 만 23세
신체정보: 174.1cm / 55.9kg / 34-26-36
출신정보: 한서대학교
전공: 항공관광학과
취미: 요리
특기: 수영
장래희망: 승무원
 피현지(PI HYUN JI)/2017년 2차 통과자
 지역: 인천
나이: 만 21세
신체: 173.9cm / 46.6kg / 34-24-36
출신: 인하공업전문대학 항공운항과
취미: 조깅
특기: 필라테스
 한호정(1991년)/2015년 미스코리아 미
 출생: 1991년 8월 20일
학력: 조지아 주립대학교 호텔경영학과
신체: 171cm/56kg
가족: 부모님
종교: 개신교
성좌/지지: 사자자리/양띠
경력: 2015년 비 어스 코리아
2015년 미스코리아 미
